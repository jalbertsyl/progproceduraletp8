/*!
\file main.c
\autor Jalbert Sylvain
\version 1
\date 20 novembre 2019
\brief fichier principal du programme qui peut crypter de 3 manières differantes un message
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cryptage.h"
#include "saisie.h"

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 20 novembre 2019
\brief la fonction principale
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES
  char *tchar_message; //le message à chiffrer
  char *tchar_resultat; //le resultat du chiffrement
  size_t int_tailleAlloue; //taille alloué en mémoire
  int int_decalage; //le nombre de décalage pour le code de César
  char *tchar_cle; //le mot clé pour le code de Vigenère

  //INITIALISATION DES VARIABLES
  tchar_message = NULL;
  tchar_cle = NULL;

  //INITIALISATION DE L'ECRAN
  system("clear");

  //si le nombre d'argument est different de 2
  if(argc < 2 || argc > 2){
    //affichage de l'aide pour expliquer à l'utilisateur comment utiliser le programme
    printf("Trois chiffrement sont disponible :\n\tcesar\n\tvigenere\n\tscytale\n\nVeuillez donner en argument le nom du chiffrement souhaité !\n\n Par exemple : ./tp8 cesar\n\n");
  }
  else{
    //Demande de saisie du message à crypter
    printf("Entrer le message à crypter : ");
    //Saisie du message à crypter et stockage de la taille du message
    getline(&tchar_message, &int_tailleAlloue, stdin);
    //si l'utilisateur à donné en argument "cesar"
    if(!strcmp(argv[1],"cesar")){
      //demander la saisie du nombe de décalage
      printf("Entrer le nombre de décalage à effectuer : ");
      //recuperer la saisie du nombe de décalage
      int_decalage = saisirEntier();
      //cryptage du message / création du tableau de résultat
      tchar_resultat = codeDeCesar(tchar_message, int_decalage);
      //affichage du resultat
      printf("Code de César...\nVoici le resultat : %s\n", tchar_resultat);
      //liberer la mémoire alloué pour le résultat
      free(tchar_resultat);
    }
    else{
      //si l'utilisateur à donné en argument "vigenere"
      if(!strcmp(argv[1],"vigenere")){
        //demander la saisie de la clé de décalage
        printf("Entrer le mot clé (En majuscule): ");
        //recuperer la saisie de la clé
        getline(&tchar_cle, &int_tailleAlloue, stdin);
        //cryptage du message / création du tableau de résultat
        tchar_resultat = codeDeVigenere(tchar_message, tchar_cle);
        //affichage du resultat
        printf("Code de Vigenère...\nVoici le resultat : %s\n", tchar_resultat);
        //liberer la mémoire alloué pour le résultat et la clé
        free(tchar_resultat);
        free(tchar_cle);
      }
      else{
        //si l'utilisateur à donné en argument "scytale"
        if(!strcmp(argv[1],"scytale")){
          //cryptage du message / création du tableau de résultat
          tchar_resultat = codageDeScytale(tchar_message);
          //affichage du resultat
          printf("Codage de Scytage...\nVoici le resultat : %s\n", tchar_resultat);
          //liberer la mémoire alloué pour le résultat
          free(tchar_resultat);
        }
        else{
          //informer que le chiffrement demandé n'est pas connu par le programme
          printf("Le chiffrement %s est inconnu !\n\nTrois chiffrement sont disponible :\n\tcesar\n\tvigenere\n\tscytale\n\n", argv[1]);
        }
      }
    }
    //libérer la mémoire aloué pour le message
    free(tchar_message);
  }
  
  //Fin du programme, Il se termine normalement, et donc retourne 0
  return(0);
}
