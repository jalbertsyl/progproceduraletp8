/*!
\file cryptage.h
\autor Jalbert Sylvain
\version 1
\date 22 novembre 2019
\brief le fichier qui contient les déclarations de toutes les méthodes de cryptage
*/

#ifndef __CRYPTAGE_H_
#define __CRYPTAGE_H_

#include <string.h>

#include "tableau.h"

/*!
\fn char *codeDeCesar ( char *tchar_message , int int_decalage )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode du code de césar
\param tchar_message le pointeur vers la premiere case du message à encoder
\param int_decalage le décalage qui sera effectué
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codeDeCesar(char *tchar_message, int int_decalage);

/*!
\fn char *codeDeVigenere ( char *tchar_message , char *tchar_cle )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode du code de Vigenère
\param tchar_message le pointeur vers la premiere case du message à encoder
\param tchar_cle la clé qui sera utilisé pour crypter le message
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codeDeVigenere(char *tchar_message, char *tchar_cle);

/*!
\fn char *codageDeScytale ( char *tchar_message)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 24 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode de codage de Scytale
\param tchar_message le pointeur vers la premiere case du message à encoder
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codageDeScytale(char *tchar_message);

#endif
