/*!
\file cryptage.c
\autor Jalbert Sylvain
\version 1
\date 22 novembre 2019
\brief le fichier qui contient les définitions de toutes les méthodes de cryptage
*/

#include "cryptage.h"

/*!
\fn char *codeDeCesar ( char *tchar_message , int int_decalage )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode du code de césar
\param tchar_message le pointeur vers la premiere case du message à encoder
\param int_decalage le décalage qui sera effectué
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codeDeCesar(char *tchar_message, int int_decalage){
  //DECLARATION DES VARIABLES
  int int_i; //variable qui va parcourir toute la chaine de caractère.
  int int_taille; //la longueur de la chaine de caractère
  char *tchar_resultat; //le resultat de l'encodage du message tchar_message

  //INITIALISATION DES VARIABLES
  //la taille est récupéré avec la fonction
  int_taille = strlen(tchar_message);

  //CREATION DE LA CHAINE DE CARACTERE tchar_resultat
  tchar_resultat = creerTableauChar(int_taille+1);

  //CRYPTAGE DU MESSAGE
  //parcourir tout le tableau
  for(int_i = 0 ; int_i < int_taille-1 ; int_i++){
    //verifier si le caractère courant est une lettre majuscule
    if(tchar_message[int_i] >= 65 && tchar_message[int_i] < 91){
      //incrémentation du caractère courant, le resultat est mis dans la case courante de tchar_resultat
      tchar_resultat[int_i] = ( tchar_message[int_i] - 65 + int_decalage ) % 26 + 65;
    }
    else{
      //verifier si le caractère courant est une lettre minuscule
      if(tchar_message[int_i] >= 97 && tchar_message[int_i] < 123){
        //incrémentation du caractère courant, le resultat est mis dans la case courante de tchar_resultat
        tchar_resultat[int_i] = ( tchar_message[int_i] - 97 + int_decalage ) % 26 + 97;
      }
      //sinon, si le caractère courant n'est pas une lettre
      else{
        //on recopie le caractère suivant dans le résultat
        tchar_resultat[int_i] = tchar_message[int_i];
      }
    }
  }

  //AJOUT DU CARACTERE DE FIN
  tchar_resultat[int_taille] = '\0';

  //RETOURNER LE POINTEUR DU RESULTAT
  return(tchar_resultat);
}

/*!
\fn char *codeDeVigenere ( char *tchar_message , char *tchar_cle )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode du code de Vigenère
\param tchar_message le pointeur vers la premiere case du message à encoder
\param tchar_cle la clé qui sera utilisé pour crypter le message
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codeDeVigenere(char *tchar_message, char *tchar_cle){
  //DECLARATION DES VARIABLES
  int int_i; //variable qui va parcourir toute la chaine de caractère du message et du résultat.
  int int_j; //variable qui va parcourir toute la chaine de caractère de la clé.
  int int_decalage; //le décalage qui sera effectué
  int int_tailleCle; //la longueur de la chaine de caractère de la clé
  int int_tailleMessage; //la longueure de la chaine de caractère du message
  char *tchar_resultat; //le resultat de l'encodage du message tchar_message

  //INITIALISATION DES VARIABLES
  //On commence la clé par la premiere lettre
  int_j = 0;
  //la taille du message est récupéré avec la fonction
  int_tailleMessage = strlen(tchar_message);
  //la taille de la clé est récupéré avec la fonction
  int_tailleCle = strlen(tchar_cle);

  //CREATION DE LA CHAINE DE CARACTERE tchar_resultat
  tchar_resultat = creerTableauChar(int_tailleMessage+1);

  //CRYPTAGE DU MESSAGE
  //parcourir tout le tableau
  for(int_i = 0 ; int_i < int_tailleMessage-1 ; int_i++){
    //définir le décalage courant
    int_decalage = tchar_cle[int_j] - 65;
    //verifier si le caractère courant est une lettre majuscule
    if(tchar_message[int_i] >= 65 && tchar_message[int_i] < 91){
      //incrémentation du caractère courant, le resultat est mis dans la case courante de tchar_resultat
      tchar_resultat[int_i] = ( tchar_message[int_i] - 65 + int_decalage ) % 26 + 65;
      //on passe à la lettre de la clé suivante (on revient à 0 si le compreur est arrivé au bout de la chaine)
      int_j = ( int_j == int_tailleCle-2 ? 0 : int_j+1 );
    }
    else{
      //verifier si le caractère courant est une lettre minuscule
      if(tchar_message[int_i] >= 97 && tchar_message[int_i] < 123){
        //incrémentation du caractère courant, le resultat est mis dans la case courante de tchar_resultat
        tchar_resultat[int_i] = ( tchar_message[int_i] - 97 + int_decalage ) % 26 + 97;
        //on passe à la lettre de la clé suivante (on revient à 0 si le compreur est arrivé au bout de la chaine)
        int_j = ( int_j == int_tailleCle-2 ? 0 : int_j+1 );
      }
      //sinon, si le caractère courant n'est pas une lettre
      else{
        //on recopie le caractère suivant dans le résultat
        tchar_resultat[int_i] = tchar_message[int_i];
      }
    }
  }

  //AJOUT DU CARACTERE DE FIN
  tchar_resultat[int_tailleMessage] = '\0';

  //RETOURNER LE POINTEUR DU RESULTAT
  return(tchar_resultat);
}

/*!
\fn char *codageDeScytale ( char *tchar_message)
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 24 novembre 2019
\brief une procedure qui permet de crypter un message avec la méthode de codage de Scytale
\param tchar_message le pointeur vers la premiere case du message à encoder
\return tchar_resultat le pointeur vers la premiere case de la chaine de caracrère qui contient le résultat après l'encodage
*/
char *codageDeScytale(char *tchar_message){
  //DECLARATION DES VARIABLES
  int int_i; //variable qui va parcourir toutes les colonnes de la grille fictive
  int int_j; //variable qui va parcourir toutes les lignes de la grille fictive
  int int_r; //variable qui va parcourir toute la chaine de caractère du résultat
  int int_tailleMessage; //la longueur de la chaine de caractère du message
  int int_tailleResultat; //la longueur de la chaine de caractère du résultat
  int int_dimensionMessage; //les dimensions de la grille fictive du message (= longueur des lignes = longueur des colonnes)
  char *tchar_resultat; //le resultat de l'encodage du message tchar_message

  //INITIALISATION DES VARIABLES
  //la taille du message est récupéré avec la fonction
  int_tailleMessage = strlen(tchar_message) - 1;
  //Pour le calcul de la dimension, on commence à 0
  int_dimensionMessage = 0;
  //On commence le parcour du résultat à la premiere case du tableau
  int_r = 0;

  //CALCUL DE LA DIMENSION DE LA GRILLE
  //tant que le carré de la dimention est inferieur à la taille, on incrémente cette dimension
  while(int_dimensionMessage * int_dimensionMessage < int_tailleMessage){
    int_dimensionMessage++;
  }

  //CALCUL DE LA TAILLE DU RESULTAT
  int_tailleResultat = int_tailleMessage + 1 + int_tailleMessage%int_dimensionMessage;

  //CREATION DE LA CHAINE DE CARACTERE tchar_resultat
  tchar_resultat = creerTableauChar(int_tailleResultat);

  //CRYPTAGE DU MESSAGE
  //parcourir toutes les colonnes de la grille fictive
  for(int_i = 0 ; int_i < int_dimensionMessage ; int_i++){
    //parcourir toutes les cases de la colonne courante de la grille fictive
    for(int_j = int_i ; int_j < int_tailleMessage ; int_j+=int_dimensionMessage){
      //verifier si on n'a pas déppassé la longueur du message
      if(int_j < int_tailleMessage){
        //on copie le caractère courant dans le résultat
        tchar_resultat[int_r] = tchar_message[int_j];
      }
      //si on a déppassé la longueur du message
      else{
        //on ajoute un espace au résultat
        tchar_resultat[int_r] = ' ';
      }
      //Le caracrère courant du résultat a été traité, on passe au suivant
      int_r++;
    }
  }

  //AJOUT DU CARACTERE DE FIN
  tchar_resultat[int_tailleResultat] = '\0';

  //RETOURNER LE POINTEUR DU RESULTAT
  return(tchar_resultat);
}
