/*!
\file tableau.c
\autor Jalbert Sylvain
\version 1
\date 22 novembre 2019
\brief le fichier qui contient les définitions de toutes les méthodes relatives aux manipulations de tableau
*/

#include "tableau.h"

/*!
\fn char *creerTableauChar ( int int_taille )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 novembre 2019
\brief une fonction qui creer un tableau de caracrère
\param int_taille la taille du tableau à creer
\return le pointeur de la premiere case du tableau
*/
char *creerTableauChar(int int_taille){
  //DECLARATION DES VARIABLES
  char *tchar_tab; //le poiteur vers la premiere case du tableau

  //ALLOCATION DE LA MEMOIRE
  tchar_tab = malloc(int_taille * sizeof(char));
  //Si l'allocation c'est fini en echec
  if(tchar_tab == NULL){
    //Avertir l'utilisateur
    printf("Erreur d'allocation mémoire !");
    //Quitter le programme avec un message d'erreur
    exit(ERREUR_ALLOCATION);
  }

  //retourner l'addresse de la premiere case du tableau, soit : tint_tab
  return(tchar_tab);
}
